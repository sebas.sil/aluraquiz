import React from 'react'
import estyled from 'styled-components'
import PropTypes from 'prop-types'

const ButtonBase = estyled.button`
  background-color: ${({ theme }) => theme.colors.secondary};
  color: ${({ theme }) => theme.colors.contrastText};
  border-radius: ${({ theme }) => theme.borderRadius};
  border: 0;
  width: 100%;
  padding: 10px 16px;
  font-weight: bold;
  font-size: 14px;
  line-height: 1;
  text-transform: uppercase;
  outline: 0;
  transition: .3s;
  cursor: pointer;
  &:hover,
  &:focus {
    opacity: .5;
  }
  &:disabled {
    background-color: #979797;
    cursor: not-allowed;
  }
`

// eslint-disable-next-line react/jsx-props-no-spreading
const Button = ({ type, ...props }) => <div><ButtonBase type={type} {...props} /></div>

Button.propTypes = {
  type: PropTypes.oneOf(['submit', 'type', 'button']).isRequired,
}

export default Button
