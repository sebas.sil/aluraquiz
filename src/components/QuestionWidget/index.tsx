import React, {FormEvent} from "react"
import Widget from "../Widget"
import Button from "../Button"

interface QuestionWidgetProps {
    question: {
        image: string
        title: string
        description: string
        alternatives: string[]
    }
    total: number,
    index: number,
    onSubmit(evt:FormEvent):void
}

const QuestionWidget: React.FC<QuestionWidgetProps> = ({question, total, index, onSubmit}) => {

    return (
        <Widget>
            <Widget.Header>
                <h3>Pergunta {index + 1} de {total}</h3>
            </Widget.Header>

            <img style={{
                width: '100%',
                height: '150px',
                objectFit: 'cover'
            }} src={question.image} />

            <Widget.Content>
                <h2>{question.title}</h2>
                <p>{question.description}</p>

                <form action="#" onSubmit={onSubmit}>
                    {
                        question.alternatives.map((a, i) => (
                            <Widget.Topic key={i} as='label' htmlFor={String(i)}><input type="radio" id={String(i)} name={String(index)} />{a}</Widget.Topic>

                        ))
                    }

                    <Button type="submit">
                        Confirmar
                    </Button>
                </form>
            </Widget.Content>
        </Widget>
    )
}

export default QuestionWidget
