import React from "react";
import Widget from "../Widget";

const LoadingWidget = () => {
    return (
      <Widget>
        <Widget.Header>
          Carregando...
        </Widget.Header>
  
        <Widget.Content style={{ display: 'flex', justifyContent: 'center' }}>

        </Widget.Content>
      </Widget>
    );
  }

  export default LoadingWidget
