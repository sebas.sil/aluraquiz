import React, {FormEvent, useEffect, useState} from 'react'

import db from '../db.json'
import QuestionWidget from '../src/components/QuestionWidget'
import GitHubCorner from '../src/components/GitHubCorner'
import QuizBackground from '../src/components/QuizBackground'
import QuizContainer from '../src/components/QuizContainer'

import QuizLogo from '../src/components/QuizLogo'
import QuizLoading from '../src/components/QuizLoading'


export default function Home() {

  const total = db.questions.length

  const [question, setQuestion] = useState(db.questions[0])
  const [index, setindex] = useState(0)

  const State = {
    QUIZ: <QuestionWidget question={question} total={total} index={index} onSubmit={onSubmit} />,
    LOADING: <QuizLoading />,
    RESULT: <div>Você acertou X questões</div>
  }

  const [state, setState] = useState('LOADING')

  useEffect(() => {
    setTimeout(() => {
      setState('QUIZ')
    }, 500);
  }, [])

  function onSubmit(evt: FormEvent) {
    evt.preventDefault()
    const current = index+1
    if(current >= total){
      setState('RESULT')
    } else {
      setQuestion(db.questions[current])
      setindex(current)
    }
    
  }

  return (
    <QuizBackground backgroundImage={db.bg}>
      <QuizContainer>
        <QuizLogo />
        {State[state]}
      </QuizContainer>

      <GitHubCorner projectUrl='https://gitlab.com/sebas.sil/aluraquiz' />
    </QuizBackground>
  )
}
