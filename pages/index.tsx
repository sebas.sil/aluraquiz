import React from 'react'
import { useRouter } from 'next/router'

import db from '../db.json'
import Widget from '../src/components/Widget'
import Footer from '../src/components/Footer'
import GitHubCorner from '../src/components/GitHubCorner'
import QuizBackground from '../src/components/QuizBackground'
import Input from '../src/components/Input'
import Button from '../src/components/Button'
import QuizContainer from '../src/components/QuizContainer'

export default function Home() {
  const router = useRouter()
  const [name, setName] = React.useState('')

  const onSubmit = (e) => {
    e.preventDefault()
    router.push(`/quiz?name=${name}`)
  }

  const onChange = (e) => setName(e.target.value)

  return (
    <QuizBackground backgroundImage={db.bg}>
      <QuizContainer>
        <Widget>
          <Widget.Header>
            <h1>{db.title}</h1>
          </Widget.Header>
          <Widget.Content>
            <form
              action="#"
              onSubmit={onSubmit}
            >
              <Input type="text" placeholder="Qual o nome do jogador?" onChange={onChange} />
              <Button type="submit" disabled={!name}>
                { /* eslint-disable-next-line react/jsx-one-expression-per-line */ }
                Vamos Jogar {name}
              </Button>
            </form>
          </Widget.Content>
        </Widget>
        <Widget>
          <Widget.Content>
            <p>lorem ipsum dolor sit amet...</p>
          </Widget.Content>
        </Widget>

        <Footer />
      </QuizContainer>

      <GitHubCorner projectUrl={db.projectUrl} />
    </QuizBackground>
  )
}
