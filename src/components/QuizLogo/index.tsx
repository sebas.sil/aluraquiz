import styled from 'styled-components';

import React from 'react';
import LogoAlura from '../../assets/logo.svg';

function Logo() {
    return (
        <LogoAlura />
    );
}

const QuizLogo = styled(Logo)`
  margin: auto;
  display: block;
  @media screen and (max-width: 500px) {
    margin: 0;
  }
`;

export default QuizLogo;
